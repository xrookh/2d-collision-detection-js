function rotate(velocity, angle) {
    return{
        x: velocity.x * Math.cos(angle) - velocity.y * Math.sin(angle),
        y: velocity.x * Math.sin(angle) + velocity.y * Math.cos(angle)
    };
}

function normalized(velocity, ball) {
    let magnitude = Math.sqrt(velocity.x**2 + velocity.y**2);
    return {
        x: (velocity.x / magnitude) * ball.speed,
        y: (velocity.y / magnitude) * ball.speed
    };
}

function resolveCollision(ball, collider) {
    let xVelocityDiff = ball.velocity.x - collider.velocity.x;
    let yVelocityDiff = ball.velocity.y - collider.velocity.y;

    let xDist = collider.x - ball.x;
    let yDist = collider.y - ball.y;

    // Prevent accidental overlap of particles
    if (xVelocityDiff * xDist + yVelocityDiff * yDist >= 0) {

        // Grab angle between the two colliding particles
        let angle = -Math.atan2(collider.y - ball.y, collider.x - ball.x);

        // Store mass in var for better readability in collision equation
        let m1 = ball.mass;
        let m2 = collider.mass;

        // Velocity before equation
        let u1 = rotate(ball.velocity, angle);
        let u2 = rotate(collider.velocity, angle);

        // Velocity after 1d collision equation
        let v1 = { x: u1.x * (m1 - m2) / (m1 + m2) + u2.x * 2 * m2 / (m1 + m2), y: u1.y };
        let v2 = { x: u2.x * (m1 - m2) / (m1 + m2) + u1.x * 2 * m2 / (m1 + m2), y: u2.y };

        // Final velocity after rotating axis back to original location
        let vFinal1 = rotate(v1, -angle);
        let vFinal2 = rotate(v2, -angle);

        // Swap particle velocities for realistic bounce effect
        ball.velocity = normalized(vFinal1, ball);
        collider.velocity = normalized(vFinal2, collider);
    }
}

function detectInterFigureCollision(ball, box) {
    let distX = Math.abs(ball.x - box.x-box.r/2);
    let distY = Math.abs(ball.y - box.y-box.r/2);

    if (distX > (box.r/2 + ball.r)) return false;
    if (distY > (box.r/2 + ball.r)) return false;

    if (distX <= (box.r/2)) return true;
    if (distY <= (box.r/2)) return true;

    let dx=distX-box.r/2;
    let dy=distY-box.r/2;
    return (dx*dx+dy*dy<=(ball.r*ball.r));
}

function createBall() {
    game.createBallFlag = true;
}
function createBox() {
    game.createBoxFlag = true;
}
