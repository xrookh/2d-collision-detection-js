"use strict";

let canvas = document.getElementById('canvas');

canvas.width = innerWidth;
canvas.height = innerHeight;

window.onresize = () => {
    canvas.width = innerWidth;
    canvas.height = innerHeight;
};
let context = canvas.getContext('2d');

let game = new Game;
iterate();
for (let i=0; i<11000; i+=1000)
{
    setTimeout(createBall,i);
}
for (let i=11000; i<21000; i+=1000)
{
    setTimeout(createBox,i);
}

function iterate()
{
    requestAnimationFrame(iterate);
    if (game.createBallFlag) {
        game.balls.push(new Ball(game.balls.length));
        game.createBallFlag = false;
    }
    if (game.createBoxFlag) {
        game.boxes.push(new Box(game.boxes.length));
        game.createBoxFlag = false;
    }
    context.clearRect(0,0,canvas.width, canvas.height);

    for (let i=0; i<game.balls.length; i++)
    {
        game.balls[i].fixedUpdate();
    }
    for (let i=0; i<game.boxes.length; i++)
    {
        game.boxes[i].fixedUpdate();
    }

}
