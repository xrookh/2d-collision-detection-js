"use strict";

class Box extends Figure {
    constructor(id)
    {
        super(id);
        this.area = this.r * this.r;
        this.x = 1;
        this.y = 1;
        console.log(this);
    }

    draw() {
        context.beginPath();
        context.rect(this.x, this.y, this.r, this.r);
        context.fillStyle = `rgba(${this.COLOR_RED}, ${this.COLOR_GREEN}, ${this.COLOR_BLUE}, 1)`;
        context.fill();
        context.closePath();
    }

    boundCollision()
    {
        if (this.x + this.r > canvas.width || this.x < 0) this.velocity.x *=-1;
        if (this.y + this.r > canvas.height || this.y < 0) this.velocity.y *=-1;
    }
    boxCollision() {
        for (let i=0; i<game.boxes.length; i++)
        {
            if (this.id === i) continue;
            if ((this.y > game.boxes[i].y+game.boxes[i].r) ||
                (this.x+this.r < game.boxes[i].x) ||
                (this.y + this.r < game.boxes[i].y) ||
                (this.x > game.boxes[i].x + game.boxes[i].r)) {}
            else
                {
                    console.log('Collision detected!');
                    resolveCollision(this, game.boxes[i]);
                }

        }
    }

}