"use strict";
// Abstract class with basic parameters
class Figure {
    x;
    y;
    r;
    speed = 3;
    mass = 1;
    velocity = {
        x:0,
        y:0
    };
    area;
    id;
    COLOR_RED;
    COLOR_BLUE;
    COLOR_GREEN;


    fixedUpdate()
    {
        this.ballCollision();
        this.boxCollision();
        this.interFigureCollision();
        this.boundCollision();
        this.move();
        this.draw();
    }
    move() {
        this.x += this.velocity.x;
        this.y += this.velocity.y;
    }
    draw(){}

    boundCollision(){}
    ballCollision(){}
    boxCollision(){}
    interFigureCollision(){}
    constructor(id)
    {
        this.id = id;
        let angle = Math.random();
        this.velocity.x = Math.cos(angle) * this.speed;
        this.velocity.y = Math.sin(angle) * this.speed;
        this.r = Math.floor(Math.random()*20)+20;
        this.x = this.r + 1;
        this.y = this.r + 1;
        // generating random color
        this.COLOR_RED = Math.floor(Math.random() * 256);
        this.COLOR_GREEN = Math.floor(Math.random() * 256);
        this.COLOR_BLUE = Math.floor(Math.random() * 256);
    }
}