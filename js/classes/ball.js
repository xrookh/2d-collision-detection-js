"use strict";

class Ball extends Figure{
    constructor(id)
    {
        super(id);
        this.area = Math.PI * this.r * this.r;
        console.log(this);
    };
    draw()
    {
        context.beginPath();
        context.arc(this.x, this.y, this.r, 0, Math.PI*2, true);
        context.fillStyle = `rgba(${this.COLOR_RED}, ${this.COLOR_GREEN}, ${this.COLOR_BLUE}, 1)`;
        context.fill();
        context.closePath();
    };

    boundCollision()
    {
        if (this.x + this.r > canvas.width || this.x - this.r < 0) this.velocity.x *=-1;
        if (this.y + this.r > canvas.height || this.y - this.r < 0) this.velocity.y *=-1;
    }

    ballCollision()
    {
        for (let i=0; i<game.balls.length; i++)
        {
            if (this.id === i) continue;
            let dist = Math.sqrt((this.x-game.balls[i].x)**2 + (this.y-game.balls[i].y)**2);
            if (dist < this.r + game.balls[i].r)
            {
                resolveCollision(this, game.balls[i]);
            }
        }
    }

    interFigureCollision() {
        for (let i=0; i<game.boxes.length; i++)
        {
            if(detectInterFigureCollision(this, game.boxes[i]))
            {
                resolveCollision(this, game.boxes[i]);
            }
        }
    }

}

